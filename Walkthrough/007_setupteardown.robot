*** Settings ***
Documentation       using command "robot -. SetupTeardown.robot"
Suite Setup         Log to console      Suite setup proceed
Suite Teardown      log to console      Suite teardown proceed
Test Setup         Log to console      Test setup proceed
Test Teardown      log to console      Test teardown proceed


*** Test Cases ***
Test201
    Log to console   test case 201 proceed

Test202
    log to console   test case 202 proceed

Test203
    [Setup]     log to console  test setup proceed from Test case section
    [Teardown]  log to console  test teardown proceed from Test case section
    log to console    test case 203 proceed
