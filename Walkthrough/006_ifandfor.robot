***Test Cases***
006_01    
     [Tags]   if
     run keyword if   2==2     log to console    2  

     @{list}   create list    a    b    c
     run keyword if   'b' in ${list}       log to console     match!!!

006_02
      [Tags]   if
      Keyword if to find company 2     4

006_03
       [Tags]   for
       FOR   ${index}    IN RANGE   0   5
             log to console     ${index}
       END
        
006_04
       [Tags]   for
       FOR   ${index}    IN RANGE   1     26      10
             log to console     ${index}
       END

006_05
       [Tags]    for 
       @{list1}   create list    a   b   c   d
       FOR    ${index}   IN RANGE   0    len(${list1})
               log to console     ${list1}[${index}]
       END

006_06
        [Tags]    for
        @{list1}   create list    a   b   c   d
        FOR      ${item}     IN      @{List1}
              log to console      ${item}
        END

006_07         #For in nested loop
    [Documentation]     For in nested loop
    [Tags]      for-loop
    FOR    ${root}    IN    r1    r2
        FOR    ${child}    IN    c1   c2    c3
            FOR    ${grandchild}    IN    g1    g2
                Log Many    ${root}    ${child}    ${grandchild}
            END
        END
        FOR    ${sibling}    IN    s1    s2    s3
                Log Many    ${root}    ${sibling}
        END
    END

006_08          #For in enumerate
    [Documentation]     For-in-enumerate
    @{list}     create list     A    B     C
    FOR    ${index}    ${item}    IN ENUMERATE    @{list}
        log to console    ${index}    
        log to console    ${item}
    END

006_09          #For in enumrate with added variable
    FOR    ${index}    ${en}    ${fi}    IN ENUMERATE
    ...                 cat      kissa
    ...                 dog      koira
    ...                 horse    hevonen
        Log to console    "${en}" in English is "${fi}" in Finnish (index: ${index})
    END

 
006_10            #For in ZIP handle Dict
    [Documentation]     For in ZIP with Dict
    &{dictData}         create dictionary   A=1     B=2     C=3
    FOR     ${key}      ${value}    IN ZIP  ${dictData.keys()}   ${dictData.values()}
            Log to Console  ${key}
            Log to Console  ${value}
    END

006_11             #For in ZIP with Dict apply with 2 dicts 
    [Documentation]     For in ZIP with Dict
    &{dictExpected}     create dictionary   A=00     B=000     C=0000
    &{dictData}         create dictionary   A=1     B=2     C=3
    FOR     ${key}      ${value}    IN ZIP  ${dictData.keys()}   ${dictData.values()}
            Log to Console  ${key}
            Log to Console  ${value}
            log to console  ${dictExpected}[${key}]
    END
   
***Keywords***
Keyword if to find company
      [Arguments]     ${data}
      run keyword if     ${data}==1     log to console   Infinitas      ELSE IF      ${data}==2     log to console   Arise


keyword if to find company 2
      [Arguments]     ${data}
      IF     ${data}==1 
             log to console    Infinitas
      ELSE IF    ${data}==2
             log to console    Arise
      ELSE  
             log to console     ktb
      END