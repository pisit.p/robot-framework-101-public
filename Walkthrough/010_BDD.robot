*** Settings ***
Documentation       Scritping style

*** Test case ***       
BDDStyle    #BDD style

  Given I Prepare "A"
  When I do "B"
  And I do another "C"
  Then I get "ABC"

*** Keywords ***
I Prepare "${something}"
    log to console      ${something}

I do "${thing}"
    log to console      ${thing}

I do another "${things}"
    log to console      ${things}

I get "${result}"
    log to console      ${result}
