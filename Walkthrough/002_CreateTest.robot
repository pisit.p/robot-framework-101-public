***Test Cases***
001 create test
   [Documentation]   create simple test case
   ${name}  Set Variable  pisit   
   ${surname}    Set Variable    P
   Log to console    ${name}_${surname}

002 create test
   Define name
   Define surname
   Combine Name and Surname then log to console

003 
   log to console    ${name}

***Keywords***
Define name
    ${name}  Set Variable  pisit   #local
    set suite variable    ${name}    ${name}

Define surname
    ${surname}    Set Variable    P
    set test variable    ${surname}

Combine Name and Surname then log to console
   Log to console    ${name}_${surname}