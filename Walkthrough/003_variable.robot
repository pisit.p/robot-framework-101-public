***Variables***
# Scalar
${Scalar1}    inf
# List
@{List1}    ktb    inf     arise
# Dictionary
&{Dict1}      name=Pisit    comp=inf

***Test Cases***
003_01
    #log to console    ${Scalar1}
    #log to console    ${List1}
    #log to console    ${Dict1}

    ${scalar2}   set variable    inf2
    @{list2}    create list    apple    orange
    &{dict2}    create Dictionary    name=tom   comp=ktb  

    log to console    ${scalar2}
    log to console    ${list2}
    log to console    ${dict2}

003_02
    log to console     ${List1}
    log to console     ${List1}[2]
    log to console     ${Dict1}[name]
    log to console     ${Dict1.comp}

003_3
    ${scalar3}   set variable    abc
    log to console    ${scalar3.upper()}

003_4
    ${var2}  set variable   ${null}
    ${var3}  set variable   ${empty}
    ${var4}  set variable   ${False}
    log to console    ${var2}
    log to console    ${var4}


003_5
    ${Scalar1}    set variable    arise
    log to console   ${Scalar1} 
