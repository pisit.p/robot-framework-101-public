***Test Cases***
Topic today
#1. Section in file
#2. Create Test case
3. Variable
    - type of variable in Robot
    - create
    - access
    - level
                global format     Xxxx_xxxx
                local format      xxxx_xxxx
    - integer variable
    - default variable
    - priority
4. keywords
    - Library Keywords
    - User keywords   # using all capital letter
    - Keyword return value
    - Keyword with argument
        - Argument as scalar
        - Argument as list
        - Argument as dict
    - Keyword with both argument and return value
    - Chain keywords
5. Common Use
    - Special Char handling
    - Comment
    - Log
    - Sleep
    - Evaluate
6. Advance
    - If with run keyword if 
    - If else, If else If
    - For in range 
    - For in {list}
    - Nested Loop
    - For in Enumerate
    - For in zip
7. Setup Teardown
    - Suite level
    - Test case level
    - Keyword level (only teardown)
8. Tags
    - Test case Tags
    - Default Tags
    - Force Tags
9. Template/ Data driven style
10. BDD style
11. Execute Test script