***Test Cases***
004_1
    ${result1}  ${result2}  Keyword return value #1
    log to console     ${result1}
    log to console     ${result2}

004_2
    keyword with 1 argument  ABC

004_3
    keyword with 2 Arguments  Test

004_4
    keyword with default argument  Pisit  

004_5
    keyword with list argument  apple     orange       coconut   

004_6 
    keyword with dictonary argument  a=1     c=2    

004_7
    ${result}    guessing number   2 
    log to console     ${result}

004_8
    ${returnformA}    Keyword a optimized   000
    log to console     ${returnformA} 

***Keywords***
Keyword return value #1
    ${return}     set variable    Trying 
    return from keyword     ${return}      Test ok

Keyword return value #2
    ${return}     set variable    Trying2 
    [Return]   ${return}


keyword with 1 argument
    [Arguments]     ${input1}
    log to console    ${input1*2}


keyword with 2 Arguments
    [Arguments]     ${input1}     ${input2}
    log to console    ${input1}_${input2}


keyword with default argument
    [Arguments]    ${input1}     ${input2}=gmail
    log to console     ${input1}@${input2}.com

keyword with list argument
    [Arguments]    @{inputlist}
    log to console      ${inputlist}
    log to console      ${inputlist}[1]

keyword with dictonary argument
    [Arguments]     &{inputDict}
    log to console     ${inputDict}[a]
    log to console     ${inputDict}[c]

guessing number 
    [Arguments]      ${number}
    return from keyword if    ${number}==5      return from keyword      Correct!!!
    return from keyword    Guessing Again!!

Keyword a
    [Arguments]    ${input}
    ${resultfromB}    keyword b  a_${input}  
    [return]     ${resultfromB}_A

keyword a optimized
    [Arguments]    ${input}
    run keyword and return     keyword b    a_${input}  

keyword b
    [Arguments]     ${inputfromA}
    [return]     ${inputfromA}_B