*** Settings ***
#  using for import Library, resource and etc
Documentation        
Library     Collections
Library     SeleniumLibrary
Resource    
Suite Setup
Suite Teardown
Test Setup
Test Teardown
Force Tags
Default Tags
Test Timeout
Test Template
    

*** Variable ***
#  define Suite variable


*** Test Cases ***
#  define test cases which robot run form this section.
    [Documentation]       Description per  Test case
    ...                   Can be use new line by ...
    [Tags]
    [Setup]        
    [Teardown]
    [Template]
    [Timeout]



*** Keywords ***
#  define user keywords using by Test cases section.
    [Documentation]      
    [Arguments]
    [Return]
    [Tags]
    [Timeout]   


***










